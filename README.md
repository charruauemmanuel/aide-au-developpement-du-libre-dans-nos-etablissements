### Aide au développement du libre dans nos établissements

## Présentation
Aide au développement du libre dans nos établissements est un dépot permettant de regrouper différents sources permettant de contacter puis de convaincre un établissement ou une mairie d'utiliser les logiciels de La Forge.


## Contenu
Vous pourrez trouver ici des modèles de lettres, des documents officiels à citer ainsi que des discussions de travail pour aider à la rédaction de nouveaux documents.
